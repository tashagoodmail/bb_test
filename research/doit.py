#!/usr/bin/env python

filename = 'ans.org'

with open(filename) as f:
    content = f.readlines()

print('{')
print('"list": [')
i = 0;
for line in content:
    if (line[0] == '+' or line[0] == '-'):
        if (i != 0):
            print(',')
        i += 1;
        print('"' + line[0:-1] + '"', end='')
    elif (line[0] == '\n'):
        print('\n]')
        print('},')
    else:
        i = 0
        print('{')
        print('"q": "' + line[0:-1] + '",')
        print('"a": [')
    
print(']')
print('}')
print(']')
print('}')
